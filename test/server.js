const test = require('blue-tape')
const { keygen } = require('..')
const { createServer } = require('https')
const { readFileSync } = require('fs')
const { join } = require('path')
const { promisify } = require('util')
const { Agent } = require('https')
const rimraf = require('rimraf')
const mkdirp = require('mkdirp')
const fetch = require('node-fetch')

function startServer (key, cert) {
  return new Promise((resolve, reject) => {
    createServer(
      {
        key: readFileSync(key),
        cert: readFileSync(cert)
      },
      (request, response) => {
        console.log(
          `${request.method} ${request.url}`
        )
        response.end('Hello, World!\n')
      }
    ).listen(
      0,
      function () {
        const { port } = this.address()
        console.log(
          `Listening at https://localhost:${port}/`
        )
        resolve(this)
      }
    )
  })
}

test('Server', async (t) => {
  const tempDir = join(__dirname, 'temp')
  await promisify(rimraf)(tempDir)
  await promisify(mkdirp)(tempDir)
  const { key, cert } = await keygen({
    entrust: false,
    key: join(tempDir, 'key.pem'),
    cert: join(tempDir, 'cert.pem')
  })

  t.ok(key)
  t.ok(cert)

  const server = await startServer(key, cert)
  const url = `https://localhost:${server.address().port}`
  const agent = new Agent({ rejectUnauthorized: false })
  const response = await fetch(url, { agent })

  t.is(response.ok, true)
  t.is(await response.text(), 'Hello, World!\n')
  server.close()
  await promisify(rimraf)(tempDir)
})
